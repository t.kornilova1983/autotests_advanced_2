
module.exports = {
  collectCoverage: true,
  coverageDirectory: "coverage",
  transform: {
    "^.+\\.js?$": require.resolve("babel-jest"),
    "^.+\\.ts?$": require.resolve("babel-jest"),
  },
  coverageReporters: ["json"],
  reporters: [
    ["jest-junit", { outputDirectory: "reports/", outputName: "junit.xml" }],
  ],
};
